﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Scorekeeper : MonoBehaviour {

	public int scorePerGoal = 1;
	private int[] score = new int[2];
	public Text[] scoreText;
	public Text playerWin;

	void Start () {    
		// subscribe to events from all the Goals
		Goal[] goals = FindObjectsOfType<Goal> ();

		for (int i = 0; i < goals.Length; i++) {
			goals[i].scoreGoalEvent += OnScoreGoal;
		}

		for (int i = 0; i < score.Length; i++) {
			score [i] = 0;
			scoreText [i].text = "0";
		}
	}

	public void OnScoreGoal(int player) {
		score[player] += scorePerGoal;
		scoreText[player].text = score[player].ToString ();
		Debug.Log("Player " + player + ": " + score [player]);

		if (score [player] == 1) {
			int playerId = player + 1;
			playerWin.text = "Player " + playerId + " wins!";	

			Time.timeScale = 0.0f;
		}
	}
}
