﻿using UnityEngine;
using System.Collections;



public class AIPaddle : MonoBehaviour {
	//public MovePaddle2 player;
	private Rigidbody rigidbody;
	private Transform target;
	public Transform point1;
	public Transform point2;

	// Use this for initialization
	void Start () {
		//MovePaddle2 player = FindObjectOfType<MovePaddle2>();
		rigidbody = GetComponent<Rigidbody>();
		rigidbody.useGravity = false;
		target = point1.transform;
	}

	// Update is called once per frame
	void Update () {
		Vector2 direction = target.position - transform.position;

		//Debug.Log ("direction is : " + direction);





		direction = direction.normalized;
		//Debug.Log ("direction is : " + direction);


		if (direction.y < 0) {
			if (target == point1.transform)
				target = point2.transform;
			
		} else {
			if (target == point2.transform)
				target = point1.transform;
		}

		//Debug.Log ("target is " + target + " Direction is " + direction);
		//Debug.Log ("velocity is " + rigidbody.velocity.magnitude);
		//Vector2 velocity = direction * 5.0f;
		//transform.Translate(velocity * Time.deltaTime);
		rigidbody.AddForce(direction * 2.5f);

		if(rigidbody.velocity.magnitude > 2.5f)
		{
			rigidbody.velocity = rigidbody.velocity.normalized * 2.5f;
		}
	}
}
